<?php
$sdd_db_host='localhost';
$sdd_db_name='programregionstates';
$sdd_db_user='root';
$sdd_db_pass='';

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$link = new mysqli("127.0.0.1", $sdd_db_user, $sdd_db_pass, $sdd_db_name);
$link->set_charset("utf8mb4");

$category = 'Новости';

//запрос 5 последних новостей, отсортированных по дате публикации
$sql = "SELECT a.id, a.date_public, a.title, a.text, a.preview FROM articles a JOIN categories c  on  a.id_cat = c.id where c.name = '$category' order by a.date_public DESC limit 5";
$result = $link->query($sql);
while($row = $result->fetch_assoc())
{
    //тестовый вывод для проверки выборки
    //echo '<p>'.$row['id'].' '.$row['date_public'].' '.$row['title'].' '.$row['text'].' '.$row['preview'].'</p>';

    $arr = array(
        'id' => $row['id'],
        'date_public' => $row['date_public'],
        'title' => $row['title'],
        'text' => $row['text'],
        'preview' => $row['preview']
    );
    echo json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK).'<br>';
}